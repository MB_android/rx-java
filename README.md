# Rx-java

# Model.java
It's a sample of common RxJava Model linked with activity as view, represent handling activity data request on background threads and return results on activity presenter. This model used Retrofit as RxObservable provider and view presenter as executor.

# RowsHelper.java
It's util class for changing database rows by their places and getting all queries of data in the table. The class used RxJava for request to database and getting the data and returned data on MainThread in executed class;

# UserHelper.java
It's util class to process user entity. The class also using RxJava for better performance.

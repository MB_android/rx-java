
public class RowsHelper {

    private String currentUrl;
	private DatabaseHelper databaseHelper;
    private final String TAG = getClass().getName();
  
    public RowsHelper() {

    public Observable<List<String>> makeListFromCursorViaCallable(DatabaseHelper databaseHelper, SomeActivity activity) {

        return Observable.fromCallable(new PullUrlsFromCursorCallable(databaseHelper, activity))
                .subscribeOn(Schedulers.io())
                .doOnNext(output -> Log.d(TAG, "rows is  = " + Arrays.toString(output.toArray()) + " execute in " + Thread.currentThread().getName() + "thread"))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(output -> Log.d(TAG, "rows is  = " + Arrays.toString(output.toArray()) + " execute in " + Thread.currentThread().getName() + "thread"));

    }

	 private class PullUrlsFromCursorCallable implements Callable<List<String>> {

        private final PreferenceActivity activity;

        private PullUrlsFromCursorCallable(DatabaseHelper baseHelper, PreferenceActivity activity) {
            this.activity = activity;
            databaseHelper = baseHelper;
        }

        @Override
        public List<String> call() throws Exception {
            return makeListUrlsFromCursor(activity);
        }
    }

    private List<String> makeListUrlsFromCursor(SomeActivity activity) {

		int countRows = 0;
        Cursor urlsCursor = databaseHelper.getSpinnerEntries();
       

        if (urlsCursor != null) {

            countRows = DatabaseHelper.showAllData(urlsCursor);
            Log.d(TAG, "countRows is " + countRows);
        }

        List<String> savedUrls = new ArrayList<>();

        int urlsIndex = urlsCursor.getColumnIndex(databaseHelper.getColumnSpinnerEntries());

        if (urlsCursor.getCount() != 0) {

            for (int i = 0; i < urlsCursor.getCount(); i++) {

                urlsCursor.moveToPosition(i);
                savedUrls.add(urlsCursor.getString(urlsIndex));
            }
        }

        activity.setCountRows(countRows);
        urlsCursor.close();

        return savedUrls;
    }

	public RowsHelper(DatabaseHelper databaseHelper, String currentUrl) {

        this.databaseHelper = databaseHelper;
        this.currentUrl = currentUrl;

        changeRowsIntoBackground().subscribe(() -> Log.d(TAG, "change Rows was successful"),error -> Log.d(TAG, error.getMessage()));
    }

    private Completable changeRowsIntoBackground() {

        return Completable.fromCallable(rowChangeCallable)
                .subscribeOn(Schedulers.io())
                .doOnEvent(result -> Log.d(TAG, "return " + result + ", currentThread name is " + Thread.currentThread().getName()))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent(result -> Log.d(TAG, "return result in thread name is " + Thread.currentThread().getName()));
    }

	Callable<Void> rowChangeCallable = new Callable<Void>() {

        @Override
        public Void call() throws Exception {

            changeRowsTheirPlaces(currentUrl);
            return null;
        }
    };

    private void changeRowsByPlaces(String currentInputtedUrl) throws SQLException {

        Cursor cursor = databaseHelper.getSpinnerEntries();

        List<String> listUrls = new ArrayList<>(10);

        int idIndex = cursor.getColumnIndex(databaseHelper.getColumnId());
        int urlValueIndex = cursor.getColumnIndex(databaseHelper.getColumnSpinnerEntries());

        while (cursor.moveToNext()) {

            Log.d(TAG, "_id is " + cursor.getInt(idIndex) + ", entry is " + cursor.getString(urlValueIndex));
            listUrls.add(cursor.getString(urlValueIndex));
        }

        listUrls.remove(listUrls.size() - 1);
        listUrls.add(0, currentInputtedUrl);

        StringBuilder builder = new StringBuilder();

        for (String url : listUrls) {
            builder.append(url).append('\n');
        }

        Log.d(TAG, "current state listUrls: " + '\n' + builder);

        databaseHelper.updateSpinnerEntries(listEntries);

    }
   
}

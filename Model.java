public class Model {

    private static Model instance = null;
    private final ConnectivityManager connectivityManager;
	private final String TAG = getClass().getSimpleName();

    private Model(ConnectivityManager connectivityManager) {
        this.connectivityManager = connectivityManager;
    }

    public static Model getInstance(ConnectivityManager connectivityManager) {

        if (instance == null)
            instance = new PreferenceModel(connectivityManager);

        return instance;
    }


    @CheckResult
    @NonNull
    public Observable<Token> getTokenForCredentials(String userEmail, String userPassword) {

        return RetrofitBuilder
                .provideTokenService()
                .getToken(new UserExample(userEmail, userPassword))
                .subscribeOn(Schedulers.io())
                .doOnNext(output -> Log.d(TAG, "observable flow with json token is = " + output.toString() + " execute in " + Thread.currentThread().getName() + "thread"))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(output -> Log.d(TAG, "token class in observable flow is  = " + output.toString() + " execute in " + Thread.currentThread().getName() + "thread"));
    }


    @CheckResult
    @NonNull
    Observable<User> getUserDataAndPassIntoObject(String token, User user) {


        return Observable.zip(getTokenInfo(token),
                getUserShows(token),
                new BiFunction<TokenInfo, Shows, User>() {

                    @Override
                    public User apply(@NonNull TokenInfo tokenInfo, @NonNull Shows shows) throws Exception {
                        user.setUserData(tokenInfo, shows);
                        return user;
                    }
                }).subscribeOn(Schedulers.io())
                .doOnNext(output -> Log.d(TAG, "observable flow with combined user data is = " + output.toString() + " execute in " + Thread.currentThread().getName() + "thread"))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(output -> Log.d(TAG, "observable flow with combined user data is   = " + output.toString() + " execute in " + Thread.currentThread().getName() + "thread"));

    }

    @CheckResult
    @NonNull
    private Observable<TokenInfo> getTokenInfo(String token) {

        return RetrofitBuilder
                .provideTokenInfoService(token)
                .getUserTokenInfo()
                .subscribeOn(Schedulers.io())
                .doOnNext(output -> Log.d(TAG, "observable flow with token info json is = " + output.toString() + " execute in " + Thread.currentThread().getName() + "thread"))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(output -> Log.d(TAG, "userWay class in observable flow is  = " + output.toString() + " execute in " + Thread.currentThread().getName() + "thread"));
    }

    @CheckResult
    @NonNull
    private Observable<Shows> getUserShows(String token) {

        return RetrofitBuilder
                .provideShowsService(token)
                .getUserShows()
                .subscribeOn(Schedulers.io())
                .doOnNext(output -> Log.d(TAG, "observable flow with user shows json is = " + output.toString() + " execute in " + Thread.currentThread().getName() + "thread"))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(output -> Log.d(TAG, "shows class in observable flow is  = " + output.toString() + " execute in " + Thread.currentThread().getName() + "thread"));
    }


    @CheckResult
    public boolean isNetworkConnected() {
        return connectivityManager.getActiveNetworkInfo() != null;
    }

}

public class UserHelper {

	private static Users users;
    private static final String TAG = UserHelper.class.getSimpleName();
 
    public static void saveUserData(String userEmail, String userPassword, PreferencesHelper preferencesHelper, String token) {

        Callable<Void> saveUserCallable = new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                saveUserName(userEmail, userPassword, preferencesHelper, token);
                return null;
            }
        };

        Completable completable = Completable.fromCallable(saveUserCallable)
                .subscribeOn(Schedulers.io())
                .doOnEvent(result -> Log.d(TAG, "saveUserName, currentThread name is " + Thread.currentThread().getName()))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent(result -> Log.d(TAG, "saveUserName is finished in thread name is " + Thread.currentThread().getName()));


        completable.subscribe(() -> Log.d(TAG, "user data was successfully saved!"),
                error -> Log.e(TAG, "Error while saving user data: " + error.getMessage()));

    }

    private static void saveUserName(String userEmail, String userPassword, PreferencesHelper preferencesHelper, String token) {

        try {

            preferencesHelper.saveCurrentUserEmail(userEmail);
            preferencesHelper.cacheToken(token);
            preferencesHelper.saveUserState(userEmail, userPassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public static void getCurrentUser(PreferencesHelper preferencesHelper, Subscriber<User> subscriber) {

        getAllUsers(preferencesHelper, new SimpleRxSubscriber<Users>() {
            @Override
            public void onNext(Users users) {

                try {

                    User user = findCurrentUser(preferencesHelper, users);
                    subscriber.onNext(user);
                    subscriber.onComplete();

                } catch (NullPointerException e) {
                    subscriber.onError(e);
                }

            }
        });
    }

    private static User findCurrentUser(PreferencesHelper preferencesHelper, Users users) throws NullPointerException {

        String currentUserEmail = preferencesHelper.getCurrentUserEmail();

        List<User> userList = users.getUsersList();

        User user = null;

        if (userList != null) {

            for (User counterUser : userList) {

                if (counterUser.getEmail().equals(currentUserEmail)) {
                    user = new User(counterUser.getEmail(), counterUser.getPassword());
                }

            }
        }

        return user;
    }


    public static void removeUserViaEmail(String userEmail, PreferencesHelper preferencesHelper, StateListener stateListener) {

        Callable<Void> removeUserCallable = new Callable<Void>() {

            @Override
            public Void call() throws Exception {
                removeUser(userEmail, preferencesHelper);
                return null;
            }
        };

        Completable completable = Completable.fromCallable(removeUserCallable)
                .subscribeOn(Schedulers.io())
                .doOnEvent(result -> Log.d(TAG, "remove user email, currentThread name is " + Thread.currentThread().getName()))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnEvent(result -> Log.d(TAG, "remove user email is finished in thread name is " + Thread.currentThread().getName()));


        completable.subscribe(() -> {

                    Log.d(TAG, "user was successfully removed!");
                    stateListener.onOperationSuccess();

                },
                error -> {

                    Log.e(TAG, "Error while removing user : " + error.getMessage());
                    stateListener.onOperationFailed(error.getMessage());

                });
    }

    private static void removeUser(String userEmail, PreferencesHelper preferencesHelper) throws JSONException {

        preferencesHelper.removeUser(userEmail);
    }


    public static void getAllUsers(PreferencesHelper preferencesHelper, Subscriber<Users> subscriber) {

        Callable<HashMap<String, String>> usersCallable = new Callable<HashMap<String, String>>() {

            @Override
            public HashMap<String, String> call() throws Exception {
                return getAllUsersFromPreferences(preferencesHelper);
            }
        };

        Observable<Users> observable = Observable.fromCallable(usersCallable).subscribeOn(Schedulers.io())
                .doOnNext(output -> Log.d(TAG, "users map in observable from prefs is  = " + output.toString() + " execute in " + Thread.currentThread().getName() + "thread"))
                .map(UserHelper::getModifiedUsersList)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(output -> Log.d(TAG, "users list in observable is  = " + output.toString() + " execute in " + Thread.currentThread().getName() + "thread"));

        observable.subscribe(subscriber::onNext, error -> Log.d(TAG, "Something went wrong when rx trying get all users, error is: " + error.getMessage()));
    }


    private static HashMap<String, String> getAllUsersFromPreferences(PreferencesHelper preferencesHelper) throws JSONException {
        return preferencesHelper.getAllUsers();
    }

    private static Users getModifiedUsersList(HashMap<String, String> usersMap) {

        List<User> tempUsersList = new ArrayList<User>();

        for (Map.Entry<String, String> entry : usersMap.entrySet()) {

            String userEmail = entry.getKey();
            String userPassword = usersMap.get(userEmail);

            tempUsersList.add(new User(userEmail, userPassword));
        }

        return new Users(tempUsersList);
    }

}
